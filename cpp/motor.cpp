//
//  motor.cpp
//  DirtyBot

#include "motor.h"

 /* constructor method */
Motor::Motor(double speed, double slipAngle, double distance, int tick, double throttle, double speedLimit, double limitSlipAngle, double speedGrowth, double angleGrowth, int pieceIndex)
{
    Motor::speed = speed;
    Motor::slipAngle = slipAngle;
    Motor::distance = distance;
    Motor::tick = tick;
    Motor::throttle = throttle;
    Motor::speedLimit = speedLimit;
    Motor::limitSlipAngle = limitSlipAngle;
    Motor::speedGrowth = speedGrowth;
    Motor::angleGrowth = angleGrowth;
    Motor::pieceIndex = pieceIndex;        /* index to the piece at that moment */

}



