//
//  gazoz.h
//  DirtyBot
#ifndef __DirtyBot__gazoz__
#define __DirtyBot__gazoz__

#include <iostream>
#include <deque>
#include "model.h"
#include "motor.h"
#include "Line.h"


typedef enum {
    none,
    dur,
    calc
}angleProblemEnum;

typedef struct {
    double k; //dragConstant
    double m; //mass
    double c; //max centrifugal force
}Physics;


class Gazoz {
    
    Race myRace;
    std::deque<Piece>myPiecesDeque;
    std::deque<CarPosition> myCarPositionDeque;
    std::deque<Motor> myMotorDeque;
    std::deque<Line> possibleLines;
    int T;
    int NCPindex;

public:
    
    /* init */
    Gazoz();
    Gazoz(Race);
    
    
    /* Physics */
    Physics physics;
    
    /* solvers */
    double run(CarPosition);
    int switcher();
    
    /* flags */
    bool isCrashed;
    bool isNewPiece();
    bool gazKes;
    
    
    /* loops */
    void makePossibleLines();

private:
    
    //Physics
    void estimatePhysics();
    
    
    //Counter to calculate drag constant
    int initialSpeedRecordsCounter;
    
    /* */
    angleProblemEnum angleProblem;
    
    /* output */
    void printLabels();
    void printer();
    
    /* math helpers */
    double calculateL();
    int nextArc();
    Piece* nextArcPiece();
    double estimateRadius(Piece*);
    double nextArcDistance();
    double nextArcChainDistance();
    double arcLength(Piece*);
    double calculateSpeed();
    
    double estimateSpeed();
    double estimateThrottle();
    double estimateLimitSpeed(Piece*);
    double calculateSpeedGrowth(double);
    double calculateAngleGrowth(double);
    void increaseT();
    double optimizeThrottle();
    double startSpeedGrowth;
    
    /* pointers */
    Piece* prvsPiece();
    Piece* nextPiece();

    CarPosition* prvsCarPosition();
    Motor* curMotor();
    Motor* motorAt(int);
    CarPosition* curCarPosition();
    Piece* curPiece();
    CarPosition* carPositionAt(int);

};






#endif /* defined(__DirtyBot__gazoz__) */
