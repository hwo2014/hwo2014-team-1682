//
//  gazoz.cpp
//  DirtyBot
#include "protocol.h"
#include "gazoz.h"
#include <cmath>
#include <iostream>
#include <iomanip>


/* have fun */



void Gazoz::estimatePhysics()
{
    
    double h = 1;
    double v1 = motorAt(2)->speed;
    double v2 = motorAt(1)->speed;
    double v3 = motorAt(0)->speed;
    
    
    //drag constant
    double k;
    
    k = (v1 - (v2 -v1)) / (pow(v1,2) * h);
    
    physics.k = k;
    
    //mass
    double m;
    
    m = 1.0 / ( log( ( v3 - h/k ) / ( ( v2 - h/k ))) / -k );
    
    
    physics.m = m;
    
    //TODO:learn this!!
    physics.c = 0;
    
    double terminalSpeed = h/k;
    
    std::cout << terminalSpeed << std::endl;
    
}

double Gazoz::estimateSpeed()
{
    int t = 1;
    double h = motorAt(0)->throttle;
    double v;
    double v0 = motorAt(0)->speed;
    double k = physics.k;
    double m = physics.m;
    
    
    v = ( v0 - h/k ) * exp(-k * t / m) + h/k;
    if ( v > 7.9 ) {
        //
    }
    return v;
}

//estimate limit speed for current piece
double Gazoz::estimateLimitSpeed(Piece* piece)
{
    double limit;
    double c = physics.c;
    double r = estimateRadius(piece);
    
    limit = sqrt(c * r);

    return  limit;
    
}


double Gazoz::estimateThrottle()
{
    double thro;
    double k = physics.k;
    double m = physics.m;
    double vt = estimateLimitSpeed(curPiece());
    int t = 1;
    double v0 = curMotor()->speed;
    

    
    //h = ( k * ( v(t) * e^ ( ( k * t ) / m ) - v(0) ) / ( e^ ( (k * t) /m ) - 1.0 ) )
    
    thro = ( k * ( vt * exp((k * t) / m ) - v0 ) / ( exp( (k * t) / m ) - 1.0 ) );
    
    if (thro >= 1) {
        return 1;
    } else {
        return thro;
    }
    
    
}


double Gazoz::optimizeThrottle()
{
    
    double speed = curMotor()->speed;                   /* distance/tick */
    double slipAngle = curMotor()->slipAngle;           /* drift angle of the car */
    double distance = curMotor()->distance;             /* distance to first selected target */
    int tick = curMotor()->tick;                        /* time in ticks not sure sync with server */
    double speedLimit = curMotor()->speedLimit;         /* explicit speed limit at the start of a curve */
    double limitSlipAngle = curMotor()->limitSlipAngle; /* calculated? slip angle */
    double speedGrowth = curMotor()->speedGrowth;       /* speed - previousMotor.speed // acceleration */
    double angleGrowth = curMotor()->angleGrowth;       /* angle - previousMotor.angle // how fast are we drifting */
    
    double betaPlus = 0.05;
    double gamaPlus = 0.1;
    double betaMinus =0.1;
    double gamaMinus =0.2;
    
    double myThro = 0;
    
    /* if piece is straight */
    if (curPiece()->isCurved==false) {
        
        /* straight piece */
        
        /* #tick to target distance with No throttle */
        
        
        /* this may vary track charac!!! */
        /* check if speed growth factor of track is initialized */
        /* if only car starts moving for the first time*/
        if (startSpeedGrowth == 0) {
            startSpeedGrowth = curMotor()->speedGrowth;
        }
        
        /* TODO:define max throttle for turbo */
        /* find next speeed with full thro */        /* speed in the future (1 tick later) */
        
        double nextSpeedWithFullThro = curMotor()->speed + curMotor()->speedGrowth * ( 1 -  startSpeedGrowth ); /*  track charactestic done gibi */
        
        /* L distance in the future (1 tick later) */
        //        double nextDistance = curMotor()->distance - ( curMotor()->speed + nextSpeedWithFullThro )/2;
        
        double nextDistance = curMotor()->distance - ( curMotor()->speed + nextSpeedWithFullThro )/2;
        
        /* estimate if the #tick  to target if next speed is with full throttle  assuming the speed growth remain as curren growth*/
        int nextTickToTarget = nextDistance / nextSpeedWithFullThro;
        
        /* #tick to target distance with next speed when No throttle */
        
        /* safe curve entrance speed to give up throttle */
        if ( nextSpeedWithFullThro * pow( (1 - startSpeedGrowth ), nextTickToTarget ) > curMotor()-> speedLimit ) {
            
            gazKes = true;
            return 0;
            
        } else {
            
            return 1;
        }
        
    } else {
        
        /* Piece is curved */
        /* reset gazKes flag */
        gazKes = false;
        
        double myCurAngleGrowth = curMotor()->angleGrowth;  /* current angle growth */
        double myPrvsAngleGrowth = motorAt(1)->angleGrowth; /* previous angle growth */
        double angleGrowth0 = motorAt(0)->angleGrowth;
        double angleGrowth1 = motorAt(1)->angleGrowth;
        
        /* if my angle growth is positive */
        
        //TODO:abs yaptik test icin
        if (abs(curPiece()->angle) > 0) {
            
            if (abs(curMotor()->slipAngle) <= abs(motorAt(1)->slipAngle)) {
                
                if (abs(curMotor()->slipAngle) > 0) {
                    angleProblem = none;

                } else {
                    angleProblem = dur;
                }

            } else {
                
                if (abs(myCurAngleGrowth) > abs(myPrvsAngleGrowth)) {
                    
                    angleProblem = dur;
                } else {
                    
                    //Calculate
                    double N = nextArcChainDistance() / curMotor()->speed;
                    double gama = curMotor()->limitSlipAngle - N * myCurAngleGrowth - curMotor()->slipAngle;
                    
                    if (gama > 0) {
                        
                        angleProblem = calc;
                        
                        

                        myThro = estimateThrottle();
                        
                        
                        
//                        myThro = curMotor()->speed / 10 + betaPlus * gama + gamaPlus * (myPrvsAngleGrowth - myCurAngleGrowth);
//                        std::cout<< "THR_GAMA+  " << setw(5)<< myThro << "  " << std::endl;
                        
                    } else {
                        
                        myThro = estimateThrottle();
                        
                    }
                }
            }
        }
        /*
        if (curPiece()->angle < 0) {
            
            
            if (curMotor()->slipAngle >= motorAt(1)->slipAngle) {
                
                angleProblem = none;
                
            } else {
                
                if (myCurAngleGrowth < myPrvsAngleGrowth) {
                    
                    angleProblem = dur;
                    
                } else {
                    
                    //Calculate
                    double N = nextArcChainDistance() / curMotor()->speed;
                    double gama = curMotor()->limitSlipAngle + N * myCurAngleGrowth + curMotor()->slipAngle;
                    
                    if (gama > 0) {
                        
                        angleProblem = calc;
                        myThro = curMotor()->speed / 10 + betaPlus * gama - gamaPlus * (myPrvsAngleGrowth - myCurAngleGrowth);
//                        std::cout<< "THR_GAMA+" << setw(5)<< myThro << "  " << std::endl;

                        
                    } else {
                        
                        myThro = curMotor()->speed / 10 + betaMinus * gama + gamaMinus * (myPrvsAngleGrowth - myCurAngleGrowth);
//                        std::cout<< "THR_GAMA-  " << setw(5)<< myThro << "  " << std::endl;
                    }
                }
            }
        } */
        
        switch (angleProblem) {
                
            case none:
                
                std::cout << left << setw(11) << "THR_NONE" << setw(5) << 1 << "  ";
                return 1;
                break;
                
            case dur:
                std::cout << left << setw(11) << "THR_DUR" << setw(5) << 0 << "  ";
                return 0;
                break;
                
            case calc:
                std::cout << left << setw(11) <<  "OP_THRO"<< setw(5) << myThro << "  ";

                
                if (myThro <= 1) {
                    return myThro;
                } else {
                    return  1;
                }
                break;
                
            default:
                break;
        }
    }
    
    /* compiler mandates */
    return 0; // silerbpokl;asd
}

/* init with Race */
Gazoz::Gazoz(Race race)
{
    std::cout << "Gazoz init with race" <<std::endl;
    
    
    //TODO:learn this
    

    //counter
    initialSpeedRecordsCounter = -1;
    
    myRace = race;
    T = 0;
    startSpeedGrowth = 0;
    NCPindex = 0;/* what the fuck is this? */
    
    /* flags */
    isCrashed = false;
    gazKes = false;
    
    std::deque<Line> possibleLines(0);
    
    
    
    std::deque<CarPosition> tempCPDeque(2);
    myCarPositionDeque = tempCPDeque;
    
    /* seems crazy */
    /* but we need it:) */
    Motor myMotor(0,0,0,0,0,0,0,0,0, 0);
    Motor myMotor1(0,0,0,0,0,0,0,0,0, 0);
    Motor myMotor2(0,0,0,0,0,0,0,0,0, 0);
    myMotorDeque.push_front(myMotor);
    myMotorDeque.push_front(myMotor1);
    myMotorDeque.push_front(myMotor2);
    
    printLabels();
}

/* run with my car position */

Piece* Gazoz::nextArcPiece()
{
    vector<Piece>pieces = myRace.track.pieces;
    unsigned uint = nextArc();
    Piece* nextCurvedPiece = &pieces.at(uint);
    
    return nextCurvedPiece;
}

double Gazoz::run(CarPosition myCarPosition)
{
    isNewPiece();
    /* queue my car position */
    myCarPositionDeque.push_front(myCarPosition);
    
    /* local variables */
    int tick = T;
    double curSlipAngle = curCarPosition()->angle;
    double limitSlipAngle = 60; /* should be dynamic */
//    double speedLimit = estimateLimitSpeed(nextArcPiece());
    double speedLimit = 6.65;

    double L = 0; /* reset to zero */
    double thro = 0;
    double speed = 0;
    int pieceIndex = curCarPosition()->piecePosition.pieceIndex;

    
    
    if (myCarPosition.piecePosition.inPieceDistance > 0) {
        initialSpeedRecordsCounter++;

    }
    
    if (initialSpeedRecordsCounter<3) {
        
        thro = 1;
        speed = calculateSpeed();
        //increase tick, create initial motors and push to deque
        
        

        
        double speedGrowth = calculateSpeedGrowth(speed);
        double angleGrowth = calculateAngleGrowth(curSlipAngle);
        
        
        /* set */
        if (speedGrowth > 0 && startSpeedGrowth == 0) {
            startSpeedGrowth = speedGrowth;
        }

        
        
        
        /* push current Motor */
        
        Motor currentMotor( speed,  curSlipAngle,  L,  tick, thro,  speedLimit,  limitSlipAngle, speedGrowth,  angleGrowth, pieceIndex);
        myMotorDeque.push_front(currentMotor);
        
        if (initialSpeedRecordsCounter == 2) {
            estimatePhysics();//with the first three speed values
        }
        increaseT();
        
        
        
    } else {
        
        speed = estimateSpeed();
        
        if (curPiece()->isCurved == true) {
            L = nextArcChainDistance();
        } else {
            L = calculateL();
        }
        

        double speedGrowth = calculateSpeedGrowth(speed);
        double angleGrowth = calculateAngleGrowth(curSlipAngle);
        
        
        /* set */
        if (speedGrowth > 0 && startSpeedGrowth == 0) {
            startSpeedGrowth = speedGrowth;
        }
        
        
        thro = optimizeThrottle();
        
        
        /* push current Motor */
        Motor currentMotor( speed,  curSlipAngle,  L,  tick, thro,  speedLimit,  limitSlipAngle, speedGrowth,  angleGrowth, pieceIndex);
        myMotorDeque.push_front(currentMotor);
        
        /* output */
        printer();
        
        /* upgrade tick */
        increaseT();
        
        /* return throttle */
        return thro;
    }

    //initial 3 throttle
    return 1;
}

#pragma mark Switcher
bool Gazoz::isNewPiece()
{

    if (curCarPosition()->piecePosition.pieceIndex != carPositionAt(1)->piecePosition.pieceIndex) {
        
        //update c for the new piece
        //TODO:fixxxxx
        physics.c = pow(6.65, 2) / estimateRadius(curPiece());
        return true;
    }
    else {
        
        return false;
    }
}

int Gazoz::switcher()
{
    vector<Piece>* myPiecesPtr = &myRace.track.pieces;
    unsigned nextArcIndex = nextArc();
    Piece* nextArcPtr = &myRace.track.pieces.at(nextArcIndex);
    
    int nextPieceIndex = fmod(curPiece()->index + 1, myPiecesPtr->size());
    Piece nextPiece = myPiecesPtr->at(nextPieceIndex);
    
    /* if switch on */
    if ( nextPiece.isSwitch == true ) {
        
        if (nextArcPtr->angle > 0) {
            return  1; /* switch right lane */
            
        } else {
            
            return -1; /* switch left lane  */
        }
    } else {
        
        return 0;
    }
}

void Gazoz::printer()
{
    if ( true) {
        std::cout << std::endl
        << std::setprecision(5) << std::left <<std::setfill ('_')<< setw(10)
        << std::setprecision(5) << curMotor()->speed << setw(10)
        << std::setprecision(5) << curMotor()->throttle << setw(10)
        << std::setprecision(5) << curMotor()->speedGrowth << setw(10)
        << std::setprecision(5) << curMotor()->slipAngle << setw(10)
        << std::setprecision(5) << curMotor()->angleGrowth << setw(10)
        << std::setprecision(5) << curMotor()->angleGrowth - motorAt(1)->angleGrowth << setw(10)
        << std::setprecision(5) << curMotor()->distance << setw(10)
        << std::setprecision(5) << curMotor()->limitSlipAngle << setw(10)
        << std::setprecision(5) << curMotor()->speedLimit << setw(10)
        << std::setprecision(5) << curMotor()->pieceIndex << setw(10)
        << std::setprecision(5) << curPiece()->length << setw(10)
        << std::setprecision(5) << curPiece()->radius << setw(10)
        << std::setprecision(5) << curPiece()->angle << setw(6)
        << std::setprecision(5) << curPiece()->isSwitch << setw(10)
        << std::setprecision(5) << curCarPosition()->piecePosition.inPieceDistance << setw(4)
        << std::setprecision(5) << curCarPosition()->piecePosition.lane.startLaneIndex
        << std::setprecision(5) << curCarPosition()->piecePosition.lane.endLaneIndex << "  ";
    }    /*

    if ( true/* !isCrashed && curMotor()->speed != 0 ) {
        std::cout << std::endl
        << std::setprecision(5) << std::left <<std::setfill ('_')<< setw(10)
        << std::setprecision(5) << curMotor()->speed << setw(10)
        << std::setprecision(5) << curMotor()->speedGrowth << setw(10)
        << std::setprecision(5) << curMotor()->speedGrowth<< setw(10);
    }*/
}

void Gazoz::printLabels()
{
    if (true) {
        std::cout
        << std::setprecision(8) << std::left <<std::setfill ('_')<< setw(10)
        //        << thro << setw(10)
        << "speed" << setw(10)
        << "speedGrowth" << setw(10)
        << "angle" << setw(10)
        << "angleGrowth" << setw(10)
        << "angleGG"<< setw(10)
        << "distance" << setw(10)
        << "limitAng" << setw(10)
        << "speedLimit" << setw(10)
        << "pieceIndex" << setw(10)
        << "length" << setw(10)
        << "radius" << setw(10)
        << "ang" << setw(6)
        << "swtc" << setw(15)
        << "ipc" << setw(4)
        << "sti" << setw(3)
        << "eli"
        
        <<std::endl;
        
    }

}

#pragma mark Motor fire continued



double Gazoz::calculateSpeed()
{
    double d1 = curCarPosition()->piecePosition.inPieceDistance; /* current in piece distance */
    double d2 = prvsCarPosition()->piecePosition.inPieceDistance; /* previous in piece distance */
    double prvsPieceLength = prvsPiece()->length;
    
    if (isNewPiece()) {
        
    }
    
    if ((d1 - d2) >= 0) { /* no piece shift */
        
       
        return d1 - d2;
        
    } else {
        
        
        
        if (abs(prvsPieceLength - d2 + d1)>20) {
//            throw "";
        }
        /* if switched the in piece distance will get larger than the piece length */
        return d2 <= prvsPieceLength ? prvsPieceLength - d2 + d1 : d2 - prvsPieceLength + d1;
    }
}

double Gazoz::calculateL()
{
    if (myCarPositionDeque.empty()==true) {

        return 0;
        
    } else {
        CarPosition currentPosition = myCarPositionDeque.front();
        int index = currentPosition.piecePosition.pieceIndex;
        double inPieceDistance =currentPosition.piecePosition.inPieceDistance;
        double pieceLength = myRace.track.pieces.at(index).length;
        
        double L = nextArcDistance() + (pieceLength - inPieceDistance);
        
        return L;
    }
}

int Gazoz::nextArc()
{
    CarPosition currentPosition = myCarPositionDeque.front();
    int index = currentPosition.piecePosition.pieceIndex;
    vector<Piece>* pieces;
    pieces = &myRace.track.pieces;
    
    if (curPiece()->isCurved == true) {
        //
    }
    vector<Piece>::iterator myIterator;
    myIterator = pieces->begin();
    unsigned indexT = fmod(index +1, pieces->size());
    advance(myIterator, indexT);
    myIterator;
    for (vector<Piece>::iterator it = myIterator; it < pieces->end(); it++) {
        if (it->isCurved == true) {
            return it->index;
        } else {
            
        }
    }
    myIterator = pieces->begin();
//    advance(myIterator, 1);
    for (vector<Piece>::iterator it = myIterator; it < pieces->end(); it++) {
        if (it->isCurved == true) {
            return it->index;
        }
    }
    return 0;
}

double Gazoz::nextArcDistance()
{
    vector<Piece>pieces = myRace.track.pieces;
    CarPosition currentPosition = myCarPositionDeque.front();
    unsigned uint = nextArc();
    Piece* nextCurvedPiece = &pieces.at(uint);
    
    int index = currentPosition.piecePosition.pieceIndex;
    
    double sumL = 0;
    
    
    /* vector iteration */
    
    if (nextCurvedPiece->index < index) {
        
        
        for (size_t i = index+1; i < pieces.size(); i++) {
            
            sumL += pieces.at(i).length;
        }
        
        
        for (int i = 0; i < nextCurvedPiece->index; i++) {
            
            sumL += pieces.at(i).length;
        }
        
    } else {
        
        for (int i = index+1; i < nextCurvedPiece->index; i++) {
            
            sumL += pieces.at(i).length;
        }
    }
    

    return sumL;
}

double Gazoz::nextArcChainDistance()
{
    vector<Piece>* pieces = &myRace.track.pieces;
   
    CarPosition currentPosition;
    currentPosition = myCarPositionDeque.front();
    NCPindex = currentPosition.piecePosition.pieceIndex;
    int nextNCPindex = nextArc();
    unsigned uint = nextNCPindex;
    
    Piece nextCurvedPiece = pieces->at(uint);
    
    double sumL = 0;
    
    
    vector<Piece>::iterator myIterator;
    myIterator = pieces->begin();
    unsigned indexT = fmod(curPiece()->index + 1, pieces->size());
    advance(myIterator, indexT);
    myIterator;
    for (vector<Piece>::iterator it = myIterator; it < pieces->end(); it++) {
        if (it->isCurved == true) {
            sumL+=arcLength(&(*it));
        } else {
            break;
        }
    }
//    if (isNewPiece()) {
        return sumL + curPiece()->length - curCarPosition()->piecePosition.inPieceDistance;
}

double Gazoz::arcLength(Piece* piece)
{
    if (piece->isCurved) {
        
        double arcLength = 2*M_PI*estimateRadius(piece)*(piece->angle)/360;
        return arcLength;
    }
    return 0;
}

double Gazoz::estimateRadius(Piece* piece)
{
    if (piece->isCurved) {
        CarPosition currentPosition = myCarPositionDeque.front();
        int startLaneIndex = currentPosition.piecePosition.lane.startLaneIndex;
        Lane myLane = myRace.track.lanes[startLaneIndex];
        double radius = piece->radius + myLane.distanceFromCenter;
        return radius;
    }
    return NULL;
}

#pragma mark Pointers to common objects
CarPosition* Gazoz::prvsCarPosition()
{
    unsigned backOne = 1;
    /* tryme done */
    if (myCarPositionDeque.size()<2) {
        return nullptr;
    } else {
        CarPosition* myPrvsCarPosition = &myCarPositionDeque.at(backOne); /* previous car position */ //TODO:fixmee
        return myPrvsCarPosition;
    }
}

CarPosition* Gazoz::curCarPosition()
{
    CarPosition* myCurCarPosition = &myCarPositionDeque.front(); /* current car position */
    return myCurCarPosition;
}

CarPosition* Gazoz::carPositionAt(int index)
{
    unsigned indexT = index;
    return &myCarPositionDeque.at(indexT);
}

Piece* Gazoz::prvsPiece()
//TODO:fixmee prvsPiece??
{
    vector<Piece>* myPiecesPtr = &myRace.track.pieces;
    CarPosition* myPrvsCarPositionPtr = prvsCarPosition();
    int prvsPieceIndex = myPrvsCarPositionPtr->piecePosition.pieceIndex;
    unsigned prvsPieceIndexT = prvsPieceIndex;
    Piece* prvsPiece = &myPiecesPtr->at(prvsPieceIndexT); /* still not totally covered */

    return prvsPiece;
}

Piece* Gazoz::nextPiece()
{
#warning zoozoz dene bakalim 
    //TODO:will it work?
    unsigned nextIndexT = fmod(curPiece()->index + 1, myRace.track.pieces.size());
    return &myRace.track.pieces.at(nextIndexT);
}

Piece* Gazoz::curPiece()
{
    vector<Piece>* myPiecesPtr = &myRace.track.pieces;
    int curPieceIndex = curCarPosition()->piecePosition.pieceIndex;
    unsigned curPieceIndexT = (unsigned)curPieceIndex;
    Piece* curPiece = &myPiecesPtr->at(curPieceIndexT); /* still not totally covered */
    
    return curPiece;
}

Motor* Gazoz::curMotor()
{
    return &myMotorDeque.front();
}

Motor* Gazoz::motorAt(int index)
{
    unsigned indexT = index;
    return &myMotorDeque.at(indexT);
}

#pragma mark Motor fire continued
double Gazoz::calculateSpeedGrowth(double curSpeed)
{
    return curSpeed - motorAt(0)->speed; /* used instead of front() for to remind it is the previous motor, motor deque not pushed yet */
}

double Gazoz::calculateAngleGrowth(double curAngle)
{
    return curAngle - motorAt(0)->slipAngle;
}

void Gazoz::increaseT()
{
    T++;
}

Gazoz::Gazoz ()   /* Gazoz default initializer  ' default init '*/
{
    std::cout << "Gazoz Default Constructor" <<std::endl;
}

/* end gazoz cpp */