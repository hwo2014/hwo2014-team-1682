//
//  motor.h
//  DirtyBot
#ifndef __DirtyBot__motor__
#define __DirtyBot__motor__

#include <iostream>
#include "model.h"



class Motor {
    
    
    
//    Motor* previousMotor;   /* a pointer to the previous Motor object */
    
    

public:
    Motor(double, double, double, int, double, double, double, double, double, int); /* constructor method */

    double speed;           /* distance/tick */
    double slipAngle;           /* slip angle of the car */
    double distance;        /* distance to first selected target */
    int tick;               /* time in ticks */
    double throttle;
    double speedLimit;      /* explicit speed limit at the start of a curve */
    double limitSlipAngle;       /* calculated? slip angle */
    double speedGrowth;     /* speed - previousMotor.speed // acceleration */
    double angleGrowth;     /* angle - previousMotor.angle // how fast are we drifting */
    int pieceIndex;        /* ptr to piece at that moment */
    
    
};











#endif /* defined(__DirtyBot__motor__) */
