#include "game_logic.h"
#include "protocol.h"
#include <string>
#include <array>
#include <vector>
#include "jsoncons/json.hpp"


using namespace hwo_protocol;
using jsoncons::json;
using jsoncons::pretty_print;

game_logic::game_logic()     /* constructor method */
  : action_map
    {
        { "join", &game_logic::on_join },
        { "yourCar", &game_logic::on_your_car },
        { "gameInit", &game_logic::on_game_init },
        { "gameStart", &game_logic::on_game_start },
        { "carPositions", &game_logic::on_car_positions },
        { "turboAvailable", &game_logic::on_turbo_available },
        { "crash", &game_logic::on_crash },
        { "spawn", &game_logic::on_spawn },
        { "finish", &game_logic::on_finish },
        { "lapFinished", &game_logic::on_lap_finished },
        { "gameEnd", &game_logic::on_game_end },
        { "tournamentEnd", &game_logic::on_tournament_end },
        { "error", &game_logic::on_error }
    }
{
    /* init */
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
    std::cout << "Got Car" << data << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
    std::cout << "Game Initialized" << std::endl;
    
    Race race;
    Track track;
    try {
        if (data.has_member("race")) {

            
            const jsoncons::json& raceJson = data["race"];
            const jsoncons::json& trackJson = data["race"]["track"];
        
            
            
            track.trackId = trackJson["id"].as_string();
            track.name = trackJson["name"].as_string();
            for (size_t i = 0; i < trackJson["pieces"].size(); i++) {
                Piece piece;
                if (trackJson["pieces"][i].has_member("switch")) {
                    piece.isSwitch = trackJson["pieces"][i]["switch"].as_bool();
                } else piece.isSwitch = false;
                if (trackJson["pieces"][i].has_member("length")) {
                    piece.length = trackJson["pieces"][i]["length"].as_double();
                    piece.angle = 0;
                    piece.radius = 0;
                    piece.isCurved = false;
                } else if (trackJson["pieces"][i].has_member("angle")) {
                    piece.angle = trackJson["pieces"][i]["angle"].as_double();
                    piece.radius = trackJson["pieces"][i]["radius"].as_double();
                    piece.length = 0;
                    piece.isCurved = true;

                }
                piece.index = (int) i;
                track.pieces.push_back(piece); /* pushing from back of the vector to sync with the source index */
            }
            
            for (size_t i = 0; i < trackJson["lanes"].size(); i++) {
                Lane lane;
                lane.distanceFromCenter = trackJson["lanes"][i]["distanceFromCenter"].as_double();
                lane.index = trackJson["lanes"][i]["index"].as_int();
                track.lanes.push_back(lane); /* pushing from back of the vector to sync with the source index */
            }
            
            Position position;
            position.x = trackJson["startingPoint"]["position"]["x"].as_double(); //recheck for double <--------
            position.y = trackJson["startingPoint"]["position"]["y"].as_double(); //recheck for double <--------
            StartingPoint startingPoint;
            startingPoint.position = position;
            startingPoint.angle = trackJson["startingPoint"]["angle"].as_double();
            track.startingPoint = startingPoint;
//            /* track end
    
            race.track = track;
            
            /* init cars */
            for (size_t i = 0; i < raceJson["cars"].size(); i++) {
                Car car;
                CarId carId;
                carId.name = raceJson["cars"][i]["id"]["name"].as_string();
                carId.color = raceJson["cars"][i]["id"]["color"].as_string();
                car.carId = carId;
                Dimensions dimensions;
                dimensions.length = raceJson["cars"][i]["dimensions"]["length"].as_double();
                dimensions.width = raceJson["cars"][i]["dimensions"]["width"].as_double();
                dimensions.guideFlagPosition = raceJson["cars"][i]["dimensions"]["guideFlagPosition"].as_double();
                car.dimensions = dimensions;
                race.cars.push_back(car);
            } /* end cars */
            
            /* init race session */
//            RaceSession raceSession;
//            raceSession.laps = raceJson["raceSession"]["laps"].as_int();
//            raceSession.maxLaptimeMs = raceJson["raceSession"]["maxLapTimeMs"].as_int();
//            raceSession.quickRace = raceJson["raceSession"]["quickRace"].as_bool();
//            race.raceSession = raceSession;
            /* end race session */
            /* end race */
            this->race = race;
            std::cout<< "race set" <<std::endl;
        }
    }
    
    catch (exception& e)
    
    {
        cout << "Standard exception: " << e.what() << endl;
    }
    
    
    /* init gazoz with race ||  little bit late init but no,.. */
    Gazoz ocGazoz(race);
    this->myGazoz = ocGazoz;
    
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
    std::cout << "Game started" << std::endl;
    raceStarted = true;
    

    return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
    
    double thro = 0;
    
    for (size_t i = 0;i < data.size() ; i++) {
        CarPosition carPosition;
        carPosition.angle = data[i]["angle"].as_double();
        CarId carId;
        carId.name = data[i]["id"]["name"].as_string();
        carId.color = data[i]["id"]["color"].as_string();
        carPosition.carID = carId;
        PiecePosition piecePosition;
        /* tryme */
        piecePosition.pieceIndex = data[i]["piecePosition"]["pieceIndex"].as_int();
        piecePosition.inPieceDistance = data[i]["piecePosition"]["inPieceDistance"].as_double();
        LaneIndexes lane;
        lane.startLaneIndex = data[i]["piecePosition"]["lane"]["startLaneIndex"].as_int();
        lane.endLaneIndex = data[i]["piecePosition"]["lane"]["endLaneIndex"].as_int();
        piecePosition.lane = lane;
        piecePosition.lap = data[i]["piecePosition"]["lap"].as_int();
        carPosition.piecePosition = piecePosition;

        
        if (i == 0) { /* my car position */ //TODO:diger araclari dusun accik
            
            thro = myGazoz.run(carPosition);
        }
    
//      IS_NEWPIECE_CHECK_POINT + SWITCH CONTROL 
        
        if(myGazoz.isNewPiece() == true) {
            
            switch (myGazoz.switcher()) {
                case -1:
                    
                    return { make_request("switchLane", "Left") };
                    break;
                    
                case 0:
                    
                    return { make_throttle( thro ) };
                    break;
                    
                case 1:
                    
                    return { make_request("switchLane", "Right") };
                    break;
                    
                default:
                    
                    return { make_throttle( thro ) };
                    break;
            }
        } else {
            
            return { make_throttle( thro ) };
        }
    }
    return { make_ping() };
    /* end of on car positions */
}



game_logic::msg_vector game_logic::on_turbo_available(const jsoncons::json& data)
{
    std::cout << "Turbo Available" << std::endl;
    std::cout << data << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
    std::cout << "gazoz crashed" << std::endl;
    myGazoz.isCrashed = true; /* pause gazoz */
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data)
{
    std::cout << "gazoz spawned" << std::endl;
    myGazoz.isCrashed = false; /* pause gazoz */
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_lap_finished(const jsoncons::json& data)
{
    std::cout << "Lap finished" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_finish(const jsoncons::json& data)
{
    std::cout << "Finish" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_tournament_end(const jsoncons::json& data)
{
    std::cout << "Tournament ended" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}
