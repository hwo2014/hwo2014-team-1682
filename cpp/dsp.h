#ifndef __DirtyBot__dsp__
#define __DirtyBot__dsp__

#include <iostream>
#include <jsoncons/json.hpp>



class dsp {
    int instanceInt = 5;
    
public:
    dsp();
    double dspValue();
    double tick(const jsoncons::json& data);
    int test( int);
    void race (const jsoncons::json& data);
};



#endif /* defined(__DirtyBot__dsp__) */