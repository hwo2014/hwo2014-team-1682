//
//  model.h
//  DirtyBot

#ifndef __DirtyBot__model__
#define __DirtyBot__model__

#include <iostream>
#include <string>
#include <vector>

using namespace std;

typedef struct {
    int startLaneIndex;
    int endLaneIndex;
}LaneIndexes;

typedef struct {
    int pieceIndex;
    double inPieceDistance;
    LaneIndexes lane;
    int lap;
}PiecePosition;

typedef struct {
    string name;
    string color;
}CarId;

typedef struct {
    CarId carID;
    double angle;
    PiecePosition piecePosition;
}CarPosition;

//typedef struct {
//    int laps;
//    int maxLaptimeMs;
//    bool quickRace;
//}RaceSession;

typedef struct {
    double length;
    double width;
    double guideFlagPosition;
}Dimensions;

typedef struct {
    CarId carId;
    Dimensions dimensions;
}Car;

typedef struct {
    double x;
    double y;
}Position;

typedef struct {
    double length;
    bool isSwitch;
    double radius;
    double angle;
    int index;
    bool isCurved;
}Piece;

typedef struct {
    Position position;
    double angle;
}StartingPoint;

typedef struct {
    double distanceFromCenter;
    int index;
}Lane;


typedef struct {
    string trackId;
    string name;
    vector<Piece> pieces;
    vector<Lane> lanes;
    StartingPoint startingPoint;
}Track;


typedef struct {
    Track track;
    vector<Car> cars;
//    RaceSession raceSession;
}Race;

/* dynamic QUEUE design */


#endif /* defined(__DirtyBot__model__) */
