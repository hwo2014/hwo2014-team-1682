#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>
#include "model.h"
#include "motor.h"
#include "gazoz.h"

class game_logic
{
    Gazoz myGazoz; /* gazzzzooozzz */
    
public:
    typedef std::vector<jsoncons::json> msg_vector;

    game_logic();
    msg_vector react(const jsoncons::json& msg);
    Race race;
    bool raceStarted;
    
    
    
private:
    typedef std::function<msg_vector(game_logic*, const jsoncons::json&)> action_fun;
    const std::map<std::string, action_fun> action_map;

    msg_vector on_join(const jsoncons::json& data);
    msg_vector on_your_car(const jsoncons::json& data);
    msg_vector on_game_init(const jsoncons::json& data);
    msg_vector on_game_start(const jsoncons::json& data);
    msg_vector on_car_positions(const jsoncons::json& data);
    msg_vector on_turbo_available(const jsoncons::json& data);
    msg_vector on_crash(const jsoncons::json& data);
    msg_vector on_spawn(const jsoncons::json& data);

    msg_vector on_lap_finished(const jsoncons::json& data);
    msg_vector on_game_end(const jsoncons::json& data);
    msg_vector on_finish(const jsoncons::json& data);
    msg_vector on_tournament_end(const jsoncons::json& data);
    msg_vector on_error(const jsoncons::json& data);
};

#endif
