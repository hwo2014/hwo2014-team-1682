import json
import socket
import sys
import math

        
class NoobBot(object):
    
    #constants
    myId = 'Gazoz'

    #varibles containing game_init data
    race = 0 # list
    track = 0 # list
    track_id = 0 # string
    track_name = 0 # string
    track_pieces = 0 # array
    track_lanes = 0 # array
    track_startingPoint = 0 # list
    track_startingPoint_position = 0 # list
    track_startingPoint_position_x = 0 # number
    track_startingPoint_position_y = 0 # number
    track_startingPoint_angle = 0 # number
    cars = 0 # array
    raceSession = 0 # list
    raceSession_laps = 0 # number of laps in the race
    
    #variables derived from game_init data
    no_of_cars = 0 # number of cars in the race
    no_of_pieces = 0 # number of pieces on the track
    no_of_lanes = 0 # number of lanes on the track
    myIndex = -1 # number, my index in "cars" array
    possible_lines = 0 # list, contains all possible llines of the race as lists of sL-eL indices
    no_of_possible_lines = 0 # number
    best_line = 0 # list, the minimum length lline amongst all possible lines
    track_blocks = 0 # list that contains blocks, each block contains piece indices
    a = -1 # number, last piece index inside the first block
    straight_track = False # bool, true if race track is straight, i.e. sprint race
    # !!!!!!!!!!!!!!!!! straight_track = True case should be dealt with in on_car_positions method!!!!!!!!
    no_of_blocks = 0 # number
    longest_straight_blocks = 0
    
    #all variables concerning the game_init data are initialized in the method "on_game_init"

    #variables containing carPositions data
    carPositions = 0 # array
    me = 0 # list, contains my position data
    me_angle = 0 # number
    me_piecePosition = 0 # list
    me_pieceIndex = -1 # number
    me_piece = 0 # list
    me_inPieceDistance = -1 # number
    me_lane = -1 # number
    me_startLaneIndex = -1 # number
    me_endLaneIndex = -1 # number
    me_lap = -1 # number, my current lap in race

    #variables derived from carPositions data

    assumed_line = 0 # list, the line we currently suppose we take
    prev_assumed_line = 0 # list
    prev_pieceIndex = 0 # number
    current_blockIndex = 0 # number
    next_blockIndex1 = 0 # number
    next_blockIndex2 = 0 # number
    d_to_next_block1 = 0 #number
    d_to_next_block2 = 0 #number
    dtemp = 0 # number
    position_data = [] # list
    angle_data = [0,0,0] # list
    current_speed = 0 # number
    speed_data = [0,0,0] # list
    throttle_data = [0,0,0] # list
    first_speed = True # bool, true until first speed read, false afterwards
    first_angles = True # bool, true until first FEW????? angles are read, false afterwards
    current_limit_speed = 0 # number , this limit determines an upper bound for throttle value
    angle_problem = 'None '# string , 'None' if no angle problem, 'Dur' if angle problem requires 0 throttle , 'Calc' if it requires calc_throttle
    angle0 = 0 # number
    angle1= 0 # number, last three angles of the car
    angle2 = 0 # number
    turbo_duration = 0 # number, duration in ticks
    turbo_factor = 1 # number
    turbo_on = False # bool, True when turboAvailable msg is received
    turbo_count = -1 # number, counts during turbo boost
    turbo_not_done = True # bool, becomes False when turbo message is sent
    turbo_piece_changed = True # bool , becomes False when turbo message sent and remains so until next block
    switch_msg_not_sent = True # bool, becomes false when swtich message is sent
    there_is_switch = False # bool, True if current piece has switch data
    switch_assumed = False # bool , True if assumed line dictates a switch lane for the current piece
    left_right = 0 # number, = 1, -1 if want to turn right, left. 0 else...
    car_alert = False # bool, becomes True if cars around require a line change 
    #all variables concerning the game_init data are initialized in the method "on_car_positions"
    
    # extractes system variables

    alpha = 0.2 # number, first system constant. 0.2 was the value in Finland track
    beta_minus = math.sqrt(2) # number, the control in calc_throttle()
    beta_plus = 1/math.sqrt(2) # number, the control in calc_throttle()
    max_angle = 60 # number, maximum angle before crash in degrees
    top_speed = 10 # number, so far observed top speed.

    
    # METHODS :

    # OUR METHODS
    
    # Math Operations, Calculators, and Analyzers
    def sgn(self, teta):
        if teta > 0:
            sgnTeta =1
        else:
            sgnTeta = -1
        return sgnTeta
    
    def calc_arcLength(self, teta, radius, dfc1, dfc2):# dfc1,2 = distance from center of sL,eL 
        sgnTeta = self.sgn(teta)
        arcLength = 2*math.pi*(radius -sgnTeta* (dfc1 + dfc2)/2)*sgnTeta*teta/360
        return arcLength 

    def piece_analyzer(self, piece):
        if piece.has_key('length'):
            return "straight"
        else:
            return "curved"
        
    def calc_pieceLength(self, piece, dfc1, dfc2): 
        if "curved" == self.piece_analyzer(piece):
            arcLength = self.calc_arcLength(piece['angle'], piece['radius'], dfc1, dfc2)
            return arcLength
        elif "straight" == self.piece_analyzer(piece):
            return piece['length']

    def calc_pieceLength2(self, pieceIndex, line):
        sLeL = line[pieceIndex]
        sL = sLeL[0]
        eL = sLeL[1]
        dfc1 = self.track_lanes[sL]
        dfc1 = dfc1['distanceFromCenter']
        dfc2 = self.track_lanes[eL]
        dfc2 = dfc2['distanceFromCenter']
        piece_length = self.calc_pieceLength(self.track_pieces[pieceIndex], dfc1, dfc2 )
        return piece_length

    def calc_lineLength(self, line):
        length_data= []
        if not len(line)== len(self.track_pieces):
            print 'Error in best line algorithm'
        for x in range (0, len(line)):
            piece_length = self.calc_pieceLength2(x, line )
            length_data.append(piece_length)
        lineLength = math.fsum(length_data)
        return lineLength

    def different_pieces(self, piece1, piece2):
        if piece1.has_key('length') and piece2.has_key('length'):
            return False
        elif piece1.has_key('radius')and piece2.has_key('radius'):
            radius1 = piece1['radius']
            radius2 = piece2['radius']
            if radius1 == radius2:
                return False
            else:
                return True
        else:
            return True

    def calc_blockLength(self, blockIndex, line):
        block = self.track_blocks[blockIndex]
        lengths = block[:]
        for x in range (0, len(block)):
            pieceIndex = lengths.pop()
            lengths.reverse()
            pieceLength = self.calc_pieceLength2(pieceIndex, line)
            lengths.append(pieceLength)
            lengths.reverse()
        blockLength = math.fsum(lengths)
        return blockLength

    def calc_blockAngle(self, blockIndex):
        block = self.track_blocks[blockIndex]
        angles = block[:]
        for x in range (0, len(block)):
            pieceIndex = angles.pop()
            angles.reverse()
            piece = self.track_pieces[pieceIndex]
            if piece.has_key('angle'):
                pieceAngle = piece['angle']
            else:
                return 0
            angles.append(pieceAngle)
            angles.reverse()
        blockAngle = math.fsum(angles)
        return blockAngle

     # collects the indices of longest stragiht blocks, once in game_init
    def find_longest_straight_blocks(self, blocks):
        str_block_list = []
        longest_block_indeces = []
        # finding straight blocks
        for x in range(0, self.no_of_blocks):
            block = blocks[x]
            piece_index = block.pop()
            block.append(piece_index)
            piece = self.track_pieces[piece_index]
            if piece.has_key('length'):
                str_block_list.append(x)
        # determining longest straight block(s)
        block_index = str_block_list.pop()
        longest_block_indeces.append(block_index)
        max_length = self.calc_blockLength(block_index, self.best_line)
        for x in range (1, len(str_block_list)):
            block_index = str_block_list.pop()
            length = self.calc_blockLength(block_index, self.best_line)
            if length >  max_length :
                longest_block_indices = []
                longest_block_indices.append(block_index)
                max_length = length
            elif length ==  max_length :
                longest_block_indices.append(block_index)
        self.longest_straight_blocks = longest_block_indices

    # finds in which block the piece is
    def piece_in_block(self, pieceIndex):
        block_list = self.track_blocks
        for x in range (0, self.no_of_blocks):
            block = block_list.pop()
            for y in range (0, len(block)):
                if pieceIndex == block.pop():
                    return x

    # returns True if given blockIndex belongs to a longest straight block
    def in_longest_straight_block(self, blockIndex):
        blockList = self.longest_straight_blocks
        in_list = False # assume not in list
        for x in range (0, len(blockList)):
            if blockIndex == blockList.pop():
                in_list = True
        return in_list

    # returns true if current speed is less then some % of the current limit speed
    def turbo_allowed(self, duration):
        if self.current_speed <  self.current_limit_speed *math.exp(-duration/75):
            return True
        else:
            return False

    def distance_to_next_block(self, blockIndex, pieceIndex, inPieceDistance, line):
        block = self.track_blocks[blockIndex]
        lengths = block[:]
        count = 0
        for x in range(0,len(block) ):
            index = lengths.pop()
            lengths.reverse()
            pieceLength = self.calc_pieceLength2(index, line)
            lengths.append(pieceLength)
            lengths.reverse()
            count = count + 1
            if index == pieceIndex :
                break
        lengths = lengths[ 0 : count]
        distance = math.fsum(lengths) - inPieceDistance
        return distance

    def distance_btw_lines(self, line1, line2):
        distance_list = []
        for x in range (0, self.no_of_pieces):
            sLeL1 = line1[x]
            sL1 = sLeL1[0]
            eL1 = sLeL1[1]
            sLeL2 = line2[x]
            sL2 = sLeL1[0]
            eL2 = sLeL1[1]
            dist = abs(sL1 - sL2) + abs(eL1 - eL2)
            distance_list.append(dist)
        distance = math.fsum(distance_list)
        return distance

    # determines if given sL and pieceIndex belong to best line
    def on_best_line(self, pieceIndex, sL):
        line = self.best_line
        for x in range (0, self.no_of_pieces):
            sLeL = self.best_line[pieceIndex]
            if sL == sLeL[0]:
                return True
            else:
                return False

    # determines the closest line to the best line that contains a given sL for a given pieceIndex
    def closest_to_best_line(self, pieceIndex, sL):
        containing_lines = []
        for x in range (0, self.no_of_possible_lines):
            line = self.possible_lines[x]
            sLeL = line[pieceIndex]
            if sL == sLeL[0]:
                containing_lines.append(line)
        closest_line = containing_lines.pop()
        closest_line_distance = self.distance_btw_lines(closest_line, self.best_line)
        for x in range (1, len(containing_lines)):
            line = containing_lines.pop()
            line_distance = self.distance_btw_lines(line, self.best_line)
            if line_distance < closest_line_distance:
                closest_line_distance = line_distance
                closest_line = line
        return closest_line    

    def effective_radius(self, blockIndex, line):
        effective_radius = (180*self.calc_blockLength(blockIndex, line))/(math.pi *self.calc_blockAngle(blockIndex))
        return effective_radius

    def calc_block_limit_speed(self, blockIndex, line):
        block = self.track_blocks[blockIndex]
        pieceIndex = block.pop()
        block.append(pieceIndex)
        piece = self.track_pieces[pieceIndex]
        if self.piece_analyzer(piece) == 'straight':
            block_limit_speed = 100
        else:
            R = self.effective_radius(blockIndex, line)
            block_limit_speed = 2*math.sqrt(self.alpha*R)
        return block_limit_speed

    # takes the index of the blocks ahead, not current block
    def calc_current_limit_speed(self, blockIndex, d_to_block, line):
        prev_speed = 1
        d=0
        speed = self.calc_block_limit_speed(blockIndex, line)
        while d <= d_to_block:
            prev_d = d
            prev_speed = speed
            d = d + speed
            speed = speed / (1 - self.alpha/10)
        current_limit_speed = prev_speed + (speed - prev_speed)*(d_to_block - prev_d) / prev_speed
        return current_limit_speed

    def calc_throttle(self, angle, a0, a1, a2):
        n_bAngle = self.calc_blockAngle(self.next_blockIndex1 )
        N1 = self.d_to_next_block1 / self.current_speed
        N2 = self.d_to_next_block2 / self.current_speed
        if angle > 0 and n_bAngle < angle: # a0 > a1 and a0 - a1 <= a1 - a2
            gamma = self.max_angle / (N1*(a0 - a1)+ a0)
            throttle = self.calc_throttle2(gamma)
        if angle > 0 and n_bAngle > angle: # a0 > a1 and a0 - a1 <= a1 - a2
            gamma = self.max_angle / (N2*(a0 - a1)+ a0)
            throttle = self.calc_throttle2(gamma)
        if angle < 0 and n_bAngle > angle: # a1 > a0 and a1 - a0 <= a2 - a1
            gamma = self.max_angle / abs(N1*(a0 - a1)+ a0)
            throttle = self.calc_throttle2(gamma)
        if angle < 0 and n_bAngle < angle: # a1 > a0 and a1 - a0 <= a2 - a1
            gamma = self.max_angle / abs(N2*(a0 - a1)+ a0)
            throttle = self.calc_throttle2(gamma)
        return throttle

    def calc_throttle2(self, gamma):
        if gamma >= 1:
            throttle = self.cuurent_speed*math.pow(gamma,self.beta_plus)/(10*self.turbo_factor)
        if gamma <  1:
            throttle = self.cuurent_speed*math.pow(gamma,self.beta_minus)/(10*self.turbo_factor)
        if throttle > 1:
            throttle = 1
        return throttle

    # we adjust beta in a curved block (beta only used in curved blocks)by looking at the performance in the same block in previous laps
    #def adjust_beta(self):
        
        #body missing !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        # it is an idea to keep beta_plus*beta_minus = 1
        # and if beta_plus*beta_minus = a , then always beta_plus < sqrt(a).
    
    # end of Math Operations, Calculators, and Analyzers

    # GAME INIT METHODS
    def collect_data(self, data):
        self.race = data['race']
        self.track = self.race['track']
        self.track_id = self.track['id']
        self.track_name = self.track['name']
        self.track_pieces = self.track['pieces']
        self.track_lanes = self.track['lanes']
        self.track_startingPoint = self.track['startingPoint']
        self.track_startingPoint_position = self.track_startingPoint['position']
        self.track_startingPoint_position_x = self.track_startingPoint_position['x']
        self.track_startingPoint_position_y = self.track_startingPoint_position['y']
        self.track_startingPoint_angle = self.track_startingPoint['angle']
        self.cars = self.race['cars']
        self.raceSession = self.race['raceSession']
        self.raceSession_laps = self.raceSession['laps']
        # Derived Data
        self.no_of_cars = len(self.cars)
        self.no_of_pieces = len(self.track_pieces)
        self.no_of_lanes = len (self.track_lanes)
        #finding my index
        for x in range (0, self.no_of_cars):
            car_x = self.cars[x]
            car_id = car_x['id']
            if self.myId == car_id:
                self.myIndex = x
    
    def initialize_possible_lines(self):
        possible_lines = []
        for x in range (0, self.no_of_lanes):
            line=[]
            line.append([x,x])
            possible_lines.append(line)
        if self.track_pieces[0].has_key('switch'):
            for x in range (0, self.no_of_lanes - 1):
                line=[]
                line.append([x,x+1])
                possible_lines.append(line)
            for x in range (1, self.no_of_lanes ):
                line=[]
                line.append([x,x-1])
                possible_lines.append(line)
        return possible_lines

    def extend_line (self, line, eL, x):
        liste = []
        temp1= line[:]
        temp2= line[:]
        line.append([eL,eL])
        liste.append(line)
        if  self.track_pieces[x].has_key('switch') and eL + 1 < len(self.track_lanes):
            temp1.append([eL, eL+1])
            liste.append(temp1)
        if self.track_pieces[x].has_key('switch') and eL -1  >= 0:
            temp2.append([eL, eL-1])
            liste.append(temp2)
        return liste

    def process_lines(self, init_lines, x):
        line = []
        last_sLeL = []
        final_lines = []
        temp = init_lines[:]
        for y in range (0, len(temp)):
            line = init_lines[y]
            last_sLeL = line.pop()
            last_eL = last_sLeL[1]
            line.append(last_sLeL)
            final_lines.extend(self.extend_line(line, last_eL, x))
        return final_lines

    def find_possible_lines(self):
        init_lines = self.initialize_possible_lines()
        for x in range (1, self.no_of_pieces):
            init_lines = self.process_lines(init_lines, x)
        return init_lines

    def find_best_line(self):
        min_lineIndex = 0 # index in the possible_lines list of  the minimum length line
        min_lineLength = self.calc_lineLength(self.possible_lines[0])
        for x in range (0, len(self.possible_lines)):
            lineLength = self.calc_lineLength(self.possible_lines[x])
            if lineLength < min_lineLength :
                min_lineLength = lineLength
                min_lineIndex = x
        return self.possible_lines[min_lineIndex]

    def find_a(self):
        for x in range (0, self.no_of_pieces):
            y = math.fmod(x, self.no_of_pieces)
            y = int(y)
            z = math.fmod(x+1, self.no_of_pieces)
            z = int(z)
            piece1 = self.track_pieces [y]
            piece2= self.track_pieces [z]
            if self.different_pieces(piece1, piece2): # true if different pieces
                self.a = x # a is the index of the last piece that is similar to the first_piece
                break
        if self.a == -1:
            self.straight_track =True

    def create_blocks(self):
        track_blocks = [] 
        self.find_a()
        block = []
        for x in range (self.a +1, self.no_of_pieces + 1):
            y = math.fmod(x, self.no_of_pieces)
            y = int(y)
            z = math.fmod(x+1, self.no_of_pieces)
            z = int(z)
            piece1 = self.track_pieces [y]
            piece2 = self.track_pieces [z]
            if self.different_pieces(piece1, piece2):
                block.append(y)
                track_blocks.append(block)        
                block=[]
            else:
                block.append(y)
                if x == self.no_of_pieces:
                    track_blocks.append(block)  
        block = track_blocks.pop()
        if len(block)== 1 :
            block = []
            for x in range (0, self.a+1):
                block.append(x)
            track_blocks.reverse()
            track_blocks.append(block)
            track_blocks.reverse()
        else:
            if self.a == 0:
                track_blocks.reverse()
                track_blocks.append(block)
                track_blocks.reverse()
            else:
                for x in range (1, self.a+1):
                    block.append(x)
                track_blocks.reverse()
                track_blocks.append(block)
                track_blocks.reverse()
        return track_blocks

    def block_has_piece(self, pieceIndex): # returns blockindex
        for x in range (0, self.no_of_blocks):
            block = self.track_blocks[x]
            if pieceIndex in block:
                return x
        
# MODIFIED SYSTEM METHODS

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_switch_lane(self, data):
        self.ping()

    def switch_lane(self, l_r):
        if l_r == -1:
            self.msg("switchLane", "Left")
            self.switch_msg_not_sent = False
        if l_r == 1:
            self.msg("switchLane", "Right")
            self.switch_msg_not_sent = False
        else:
            self.switch_msg_not_sent = True

    #  after a call of the "throttle" method when in "on_car_positions" 'return' is executed
    def throttle(self, throttle):
        if throttle > 1  :
            print 'THROTTLE > 1, ERROR!!!!'
            throttle = 1
        if throttle < 0  :
            print 'THROTTLE < 0, ERROR!!!!'
            throttle = 0
        self.throttle_data.append(throttle)
        self.msg("throttle", throttle)

    def turbo_available(self, data):
        self.turbo_duration = data['turboDurationTicks']
        self.turbo_factor = data['turboFactor']
        self.turbo_on = True

    def turbo(self):
        self.msg("turbo", "go beybi go")
        self.turbo_count = self.turbo_duration
        self.turbo_not_done = False

    def on_crash(self, data):
        print("Someone crashed")
        #checking if max_angle is really 60 degrees. if not correcting max angle
        angle_list = self.angle_data
        angle = angle_list.pop()
        while angle == 0:
            angle = angle_list.pop()
        last_angle1 = angle
        last_angle2 = angle_list.pop()
        if self.max_angle > abs(last_angle1) and self.max_angle < abs(last_angle1)+ abs(last_angle1 - last_angle2):
            pass
        else:
            self.max_angle = abs(last_angle1)+ abs(last_angle1 - last_angle2)
            self.max_angle = math.floor(self.max_angle)
            #check done
            self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()


# UNCHANGED SYSTEM METHODS
            
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name, "key": self.key})
    
    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'gameInit': self.on_game_init,
            'switchLane':self.on_switch_lane,
            'crash': self.on_crash,
            'turboAvailable': self.turbo_available,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()

            
    ################### ON_CAR_POSITIONS ###################

    def on_car_positions(self, data):
        
        # OBTAINING ME_CAR POSITIONS DATA
        self.carPositions = data
        self.me = data[self.myIndex]
        self.me_angle = self.me['angle']
        self.me_piecePosition = self.me['piecePosition']
        self.me_pieceIndex = self.me_piecePosition['pieceIndex']
        self.me_piece = self.track_pieces[self.me_pieceIndex]
        self.me_inPieceDistance = self.me_piecePosition['inPieceDistance']
        self.me_lane = self.me_piecePosition['lane']
        self.me_startLaneIndex = self.me_lane['startLaneIndex']
        self.me_endLaneIndex = self.me_lane['endLaneIndex']
        self.me_lap = self.me_piecePosition['lap']
        # finding current_blockIndex
        self.current_blockIndex = self.block_has_piece( self.me_pieceIndex)

        # ME_CAR POSITIONS DATA OBTAINED

        # COLLECTING POSITION, SPEED, and ANGLE DATA
        self.prev_pieceIndex = math.fmod(self.me_pieceIndex -1 , self.no_of_pieces)
        self.prev_pieceIndex = int(self.prev_pieceIndex)
        self.position_data.append(self.me_inPieceDistance )
        self.angle_data.append(self.me_angle)
        if self.me_inPieceDistance >= self.dtemp :
            self.current_speed = self.me_inPieceDistance - self.dtemp
        else :
            self.current_speed = self.calc_pieceLength2(self.prev_pieceIndex, self.prev_assumed_line) + self.me_inPieceDistance - self.dtemp
        self.dtemp = self.me_inPieceDistance
        self.speed_data.append(self.current_speed)
        # POSITION, SPEED, and ANGLE DATA COLLECTED

        # FINDING SYSTEM CONSTANTS
        # finding alpha
        if self.first_speed :
            if self.dtemp < self.me_inPieceDistance :
                self.first_speed = False
                self.alpha = self.current_speed
                
        # FIND BETA !!!!!!!!!!!!!!!!

        # SYSTEM CONSTANTS FOUND
               
        
        # Determining assumed, prev_assumed lines, PREV_ASSUMED_LINE is still not used !!!!!!!!!!!!!!!!!!!!!!!
        self.prev_assumed_line = self.assumed_line
        
        # check if cars are around !!!!!!!!!!! 

        # if no car alert follow best possible line, if car alert follow procedure not determined!!!!!!!!!!!!!!!!!!!!!!
        if self.car_alert:
            pass
            # reaction to car alert!!!!!!!!!!!!!!!!!!!!!!!!
        else:
            if self.on_best_line(self.me_pieceIndex, self.me_startLaneIndex):
                self.assumed_line = self.best_line
            else:
                self.assumed_line = self. closest_to_best_line(self.me_pieceIndex, self.me_startLaneIndex)
        
        #LOOKING AHEAD
        # finding distances to next blocks, distance_to_next_block1, 2 are current distances to the next, second next blocks
        self.d_to_next_block1 = self.distance_to_next_block(self.current_blockIndex, self.me_pieceIndex, self.me_inPieceDistance, self.assumed_line)
        self.d_to_next_block2 = self.d_to_next_block1 + self.calc_blockLength(self.current_blockIndex + 1, self.assumed_line)
        print self.d_to_next_block1, self.d_to_next_block2
        # calculate limit speed in order to be able to give max throttle possible
        self.next_blockIndex1 = math.fmod(self.current_blockIndex + 1, self.no_of_blocks)
        self.next_blockIndex1 = int(self.next_blockIndex1)
        self.next_blockIndex2 = math.fmod(self.current_blockIndex + 2, self.no_of_blocks)
        self.next_blockIndex2 = int(self.next_blockIndex2)
        current_limit_speed1 = self.calc_current_limit_speed(self.next_blockIndex1, self.d_to_next_block1, self.assumed_line)
        current_limit_speed2 = self.calc_current_limit_speed(self.next_blockIndex2, self.d_to_next_block2, self.assumed_line)
        self.current_limit_speed = min (current_limit_speed1, current_limit_speed2)
        # I LOOKED AHEAD

        
        # SWITCH PROTOCOL STARTS
        # check if current piece has switch
        if self.me_piece.has_key('switch'):
            self.there_is_switch = True
        else:
            self.there_is_switch = False
            self.switch_msg_not_sent = True
        # check if assumed_line dictates a switch, and if yes is it left or right...
        sLeL = self.assumed_line[self.me_pieceIndex]
        sL = sLeL[0]
        eL = sLeL[1]
        self.left_right = eL - sL
        if self.left_right == 0:
            switch_assumed = False
        else:
            switch_assumed = True
        # if all ok send the switch message
        if self.there_is_switch and self.switch_assumed and self.switch_msg_not_sent:
            self.switch_lane(self.left_right)
        # SWITCH PROTOCOL ENDS


        # TURBO MANAGEMENT STARTS
        # if all ok send turbo messasge
        if self.turbo_on and self.in_longest_straight_block() and self.turbo_allowed(self.turbo_duration) and self.turbo_not_done and self.turbo_piece_changed:
            self.turbo()
            self.turbo_piece_changed =False
            self.turbo_blockIndex = self.current_blockIndex
        # count during boost and indicate when it finishes
        if self.turbo_on:
            self.turbo_count = self.turbo_count -1
            if self.turbo_count < 0 :
                turbo_on = False
                self.turbo_factor = 1
        # check if still in turbo piece
        if   not self.turbo_piece_changed:
            if not self.turbo_blockIndex == self.current_blockIndex:
                self.turbo_piece_changed = True
        # TURBO MANAGEMENT DONE

        # CHECKING ANGLE PROBLEM
        if self.me_piece.has_key('length'):
            self.angle_problem = 'None'
        else:
            self.angle_data.reverse()
            self.angle0 = self.angle_data[0]
            self.angle1 = self.angle_data[1]
            self.angle2 = self.angle_data[2]
            self.angle_data.reverse()
            radius = self.me_piece['radius']
            angle = self.me_piece['angle']
            if angle >0:
                if self.angle0 <= self.angle1:
                    self.angle_problem = 'None'
                else: 
                    if self.angle0- self.angle1 > self.angle1 - self.angle2 :
                        self.angle_problem = 'Dur'
                    else:
                        self.angle_problem = 'Calc'
            if angle < 0:
                if self.angle0 >= self.angle1:
                    self.angle_problem = 'None'
                else: 
                    if self.angle1- self.angle0 > self.angle2 - self.angle1 :
                        self.angle_problem = 'Dur'
                    else:
                        self.angle_problem = 'Calc'
        # ANGLE PROBLEM CHECKED
        

        # THROTTLE ADJUSTMENT
        if self.angle_problem == 'None':
            if self.current_speed < self.current_limit_speed:
                self.throttle(1)
                return
            else:
                self.throttle(0)
                return
        if self.angle_problem == 'Calc':
            throttle = self.calc_throttle(angle, self.angle0, self.angle1, self.angle2)
            self.throttle(throttle)
            return
        if self.angle_problem == 'Dur':
            self.throttle(0)
            return
        # THROTTLE ADJUSTMENT DONE

    #################### END OF ON_CAR_POSITIONS ####################

    #################### START OF GAME_INIT ####################

    def on_game_init(self, data):
        print 'Game Init:'
        self.collect_data(data)
        self.possible_lines = self.find_possible_lines()
        self.no_of_possible_lines = len (self.possible_lines)
        self.best_line = self.find_best_line()
        self.track_blocks = self.create_blocks()
        self.no_of_blocks = len(self.track_blocks)
        self.find_longest_straight_blocks(self.track_blocks)



    #################### END OF GAME_INIT ####################    

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
